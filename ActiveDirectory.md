## Query

Obtener el listado de Host
```powershell
> Get-ADComputer -filter *
```


## Runs commands on local and remote computers


```powershell
PS C:\>  invoke-command {get-service -name bits}

Status   Name               DisplayName
------   ----               -----------
Stopped  bits               Background Intelligent Transfer Ser...
```


Esta instruccion actua en paralelo, enviandolas a cada Host
```powershell
> Invoke-Command -ComputerName Server2022,Windows11 {Get-Service -name bits}

Status   Name               DisplayName                            PSComputerName
------   ----               -----------                            --------------
Running  bits               Background Intelligent Transfer Ser... Server2022
Running  bits               Background Intelligent Transfer Ser... Windows11
```

Ejecutar un script en host remoto
```powershell
> Invoke-Command -FilePath c:\scripts\test.ps1 -ComputerName Server01
```


Show los Log del System de los host Server2022 y Windows11
```powershell
PS C:\> Invoke-Command -ComputerName Server2022,Windows11 {Get-EventLog -LogName System -new 3} | sort timewritten | Format-Table -Property timewritten,message -AutoSize

TimeWritten         Message
-----------         -------
12/01/2024 12:33:02 The time service is now synchronizing the system time with the time source Server2022.ceh.com (ntp.d|0.0.0.0:123->10.10.1.22:123) with reference id 369166858. Current ...
12/01/2024 12:33:02 The time provider NtpClient is currently receiving valid time data from Server2022.ceh.com (ntp.d|0.0.0.0:123->10.10.1.22:123).
12/01/2024 12:33:02 NtpClient succeeds in resolving domain peer Server2022.ceh.com after a previous failure.
12/01/2024 12:39:59 The Windows Update service entered the stopped state.
12/01/2024 12:40:48 The Microsoft Account Sign-in Assistant service entered the running state.
12/01/2024 12:40:58 The Network Setup Service service entered the stopped state.
```
