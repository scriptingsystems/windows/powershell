
## DNS
Consulta DNS de type MX
```powershell
> Resolve-DnsName -Type MX google.es

Name                                     Type   TTL   Section    NameExchange                              Preference
----                                     ----   ---   -------    ------------                              ----------
google.es                                MX     300   Answer     smtp.google.com                           0

> # Filter la NameExchange Property
PS C:\> Resolve-DnsName -Type MX google.es | Select-Object -Property NameExchange

NameExchange
------------
smtp.google.com
```
