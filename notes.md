## NOTES
Actualizar la Ayuda:
```powershell
> Update-Help
```

Get help de un Cmdlet
```powershell
> Get-EventLog Get-EventLog -Examples
> Get-EventLog Get-EventLog -Full
```

Get-Help filtrar por el Cmdlet que cuentan con ayuda
```powershell
> Get-Help get-*item*

Name                              Category  Module                    Synopsis
----                              --------  ------                    --------
Get-ChildItem                     Cmdlet    Microsoft.PowerShell.M... ...
Get-ControlPanelItem              Cmdlet    Microsoft.PowerShell.M... ...
Get-Item                          Cmdlet    Microsoft.PowerShell.M... ...
Get-ItemProperty                  Cmdlet    Microsoft.PowerShell.M... ...
Get-ItemPropertyValue             Cmdlet    Microsoft.PowerShell.M... ...
Get-TestDriveItem                 Function  Pester                    ...
Get-DAEntryPointTableItem         Function  DirectAccessClientComp... ...
```

Mostrar las variables de entorno y las variables prefediniada en PowerShell
```powershell
> childitem | gci | get-childitem env:*
> Get-Variable
```

Mirar eventos:
```powershell
> Get-EventLog -LogName Application -ComputerName $env:computername -Newest 10
```



Buscar en ficheros con filtros:
```powershell
> childitem -Recurse -File -Path 'C:\Program Files\' -Include *.ps1 | select-object Fullname
```

Apagar Computer: 
```powershell
> stop-computer
```

Asign a prefix a un cmdlet
```powershell
> $s = nsn
> Import-PSSsession $s -CommandName get-process -Prefix wow
> get-wowProcess
```

Get el tamaño del disk en GB
```powershell
Get-wmiObject win32_logicaldisk -filter "DeviceID='c:'" |
select @{n='freegb'; e={$_.freespace / 1gb -as [int]}}
```


Mostrar los Method and Property de un object
```powershell
> ... | gm
> #or show Parameters
> (get-command Test-Connection).parameters
```

Get el history de instrucciones lanzadas
```powershell
> Get-History    
  Id CommandLine 
  -- -----------
   1 Get-Module -ListAvailable
   2 Get-Module    
   3 Get-Module | Where-Object Name -Like 'PSReadline'
   4 Get-Module -ListAvailable | Where-Object Name -Like 'PSReadline'
   5 Get-Process    
   6 Get-Process | Where-Object Handles -gt 900                                                                         
   7 Get-Process | Where-Object Handles -gt 900 | sort Handles                                                       
   8 Get-Service | Select-Object -Property name,status
```

Get WindowsFeature, filter por "powershell"
```powershell
PS C:\> Get-WindowsFeature -Name *powershell*
             
Display Name                                            Name                       Install State
------------                                            ----                       -------------
            [ ] Failover Cluster Module for Windows ... RSAT-Clustering-Powe...        Available
            [X] Active Directory module for Windows ... RSAT-AD-PowerShell             Installed
            [ ] Hyper-V Module for Windows PowerShell   Hyper-V-PowerShell             Available
            [ ] Remote Access module for Windows Pow... RSAT-RemoteAccess-Po...        Available
[X] Windows PowerShell                                  PowerShellRoot                 Installed
    [X] Windows PowerShell 5.1                          PowerShell                     Installed
    [ ] Windows PowerShell 2.0 Engine                   PowerShell-V2                    Removed
    [X] Windows PowerShell ISE                          PowerShell-ISE                 Installed
    [ ] Windows PowerShell Web Access                   WindowsPowerShellWeb...        Available
```


Crear un directory
```powershell
> New-Item -Type Directory C:\Users\Administrator\Documents\WindowsPowerShell
```



## Variables


Almancer una variable
```powershell
> $objServiceBits = Get-Service BITS
> $objServiceBits.Status
Running
> $objServiceBits.Stop()
> $objServiceBits.Refresh()
> $objServiceBits.Status
Stopped
> $objServiceBits.Start()
> $objServiceBits.Refresh()
> $objServiceBits.Status
Running
```


## Mostrar

```powershell
> write-host 'Hello World'
Hello World

> write-output 'Hello World'
Hello World
```

Mensaje de estados de información
```powershell
> write-warning "..."

> write-error "..."
```


## Read
```powershell
> $myComputerName = read-host "Enter a ComputerName"
Enter a ComputerName: dc-01
> $myComputerName
dc-01
```



## Parameters

Ver los parameters de un object
```powershell
> $c = get-command get-process
> $c.Parameters
```

Obtener información de un parámetro en particular
```powershell
> $c.Parameters["Name"]
```

## Pipes

Obtiene el listado de procesos, de ello filtra los nombres de procesos que complan el siguiente wildcare "p*w*", y asu ves filtra para que se muestra la columna que empieza por "na*"
```powershell
Get-process | ? name -like p*w* | % na*
```

### Condicional

Filtramos por la columna **Handles** los pid que sean mayor que **900**,y lo ordenamos por esa misma columna
```powershell
> Get-Process | Where-Object Handles -gt 900 | sort Handles
                                                                             
Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    945      38    20224      70892       1,02   5912   1 SystemSettings
    948      36     9372      26224       0,88    380   0 svchost
   1089      69    80368      76812       2,16   1244   1 SearchUI
   1616      64    25280      19836       3,02    392   1 explorer
   1768     165    56724      61496       3,19    628   0 lsass
   2210       0      192        156       9,63      4   0 System
   2407     120    54848      99488      29,30    400   0 svchost
  10406    7298   130468     128436       0,53   2456   0 dns
```

Seleccionar property de un object. Por ejemplo, si queremos mostrar los servicios que se encuentren en estado **Running**, y que solo queremos que muestren las columnas **name** y **status**
```powershell
> Get-Service | Where-Object status -eq 'Running'  | Select-Object -Property name,status

Name                    Status
----                    ------
ADWS                   Running
Appinfo                Running
AppXSvc                Running
BFE                    Running
BrokerInfrastructure   Running
BthAvctpSvc            Running
CDPSvc                 Running
CDPUserSvc_6b9b0       Running
COMSysApp              Running
CoreMessagingRegistrar Running
CryptSvc               Running
DcomLaunch             Running
Dfs                    Running
```



## Function
Ver la definición de una función
```powershell
> Get-Command get-*adcomputer
> (get-command get-remoteADComputer).definition
```

## Foreach
Recorer un array de Strings
```powershell
> $myServer = 'DC', 'S1', 'S2'
> $myServer
> $myServer | foreach{start iexplore http://$_}
```

Copiar un fichero en multiples servers
```powershell
> {c:\default.html} = "MVA and PowerShell Rocks!!!" 
> $myServer = 'DC', 'S1', 'S2'
> $myServer | foreach{copy-item c:\default.html -Destination \\$_\c$\inetput\wwwroot}
> $myServer | foreach{start iexplore http://$_}
```

Recorrer una lista de numeros
```powershell
> 1..4 | foreach {Write-Output "Number: $_"}
Number: 1
Number: 2
Number: 3
Number: 4
```


## PROCESS

Obtener el path del binary
```powershell
> calc 
> get-process calc
> get-process calc | dir
```

## COMPUTER and AD COMPUTER
Cambiar el nombre a la propiedad para que sea aceptada por otro cmdlets para este caso **get-service**
```powershell
> get-adcomputer -filter * | select -Property name,@{name='ComputerName';expression={$_.name}}

> get-adcomputer -filter * | select -Property name,@{n='ComputerName';e={$_.name}}

> get-adcomputer -filter * | select -Property name,@{n='ComputerName';e={$_.name}} | get-service -name bits
# En nuevas versiones, no hace falta hacer la convertion
> get-ADComputer -filter * | get-wmiobject win32_bios -ComputerName {$_.Name}
```


Obtener información de otros hosts
```powershell
> Get-WmiObject -Class win32_bios -ComputerName dc,s1,s2
```

Si esto lo queremos hacer con Computer del AD. -ComputerName acepta Array de String, por ello se obtiene el listado obteniendo únicamente el nombre

Observamos que es un String
```powershell
> Get-ADComputer -filter * | select -ExpandProperty name | gm 

> Get-WmiObject -class win32_bios -ComputerName (Get-ADComputer -filter * | select -ExpandProperty name)
# En nuevas versiones, no hace falta hacer la convertion
> Get-WmiObject -class win32_bios -ComputerName (Get-ADComputer -filter *).name
```

## NETWORK

Check if un port esta open
```powershell
> Test-NetConnection -Port 80 elpais.es
```

## PSSession
Guardar el valor de una variable en una misma session
```powershell
> icm -ComputerName dc {$var=2}
> icm -ComputerName dc {write-output $var}
```

Esto Guardar el valor de la variable
```powershell
> $mySession = New-PSSession -ComputerName dc
> Get-PSSession
> Invoke-command -Session $mySession {$var=2}
> Invoke-command -Session $mySession {$var}
```

Medir el Tiempo en que se ejecuta un comando
```powershell
> measure-command {invoke-command -ComputerName DC {Get-Process}}
> measure-command {invoke-command -session mySession {Get-Process}}
```


Instalar una feature en multi-server
```powershell
> $myServer = 'DC', 'S1', 'S2'
> icm -Session $myServer {Install-WindowsFeature web-server}
```


Import module ActiveDirectory, en open sessions
```powershell
> Import-PSSession -session $s -Module ActiveDirectory -Prefix remote
```


## Script


Guardamos el script con el nombre **Diskinfo.ps1**. Ejecutamos la siguiente "get-help .\Diskinfo.ps1 -Full"
```powershell
<#
.Synopsis
This is the short explanation
.Description
This is the long description
.Parameter ComputerName
This is for remote computers
.Example
Diskinfo -ComputerName remote
This is for a remote computer
#>

function Get-diskinfo{
  [CmdletBinding()]
  param(
    #Va pidiendo la información por el prompt
    [Parameter(Mandatory=$True)]
    [string[]] $ComputerName = 'localhost',
    $bogus
  )

  Get-VmiObject -ComputerName $ComputerName -class win32_logicaldisk -filter "Device"
}
```

Cargar la función en memoria ". .\Diskinfo.ps1"
```powershell
Get-diskinfo -ComputerName dc
```

Pasar la instrucción a otra variable. Para este ejemplo en la variable **getdi**
```powershell
Get-diskinfo -ComputerName dc -OutVariable getdi
```


## Module

Lista de modulos disponibles
```powershell
Get-Module -ListAvailable
```

Generar un module, y lo guardamos como **Diskinfo.psm1**
```powershell
<#
.Synopsis
This is the short explanation
.Description
This is the long description
.Parameter ComputerName
This is for remote computers
.Example
Diskinfo -ComputerName remote
This is for a remote computer
#>

function Get-diskinfo{
  [CmdletBinding()]
  param(
    #Va pidiendo la información por el prompt
    [Parameter(Mandatory=$True)]
    [string[]] $ComputerName = 'localhost',
    $bogus
  )

  Get-VmiObject -ComputerName $ComputerName -class win32_logicaldisk -filter "Device"
}
```

Importamos el module 
```powershell
import-module .\Diskinfo.psm1 -Force -Verbose
```

Recargar un modulo que hemos modificado. Primero lo removemos para luego cargarlo
```powershell
remote-module
import-module .\Diskinfo.psm1 -Force -Verbose
```

Localización de los módulos, para que carguen automáticamente
```powershell
> cat Env:\PSModulePath
#or
> $Env:\PSModulePath -split ";"
```

## Mostrar ficheros

Mostrar el contenido de un fichero. Se ha de poner la ruta absoluta del fichero
```powershell
> ${c:\jumpstart\myTest.txt}
```

Si deseamos mostrar el contenido de un fichero 
```powershell
> type myTest.txt
```

Guardar info en un fichero
```powershell
> ${c:\jumpstart\myTest.txt} = "Are You Freaking Kidding"
```

```powershell
get-content myTest.txt
```

## Export a fichero

Exportar la salida de una instrucción 
```powershell
PS C:\> $myObject = [PSCustomObject]@{
>> Nombre = "Ejemplo"
>> Edad = 25
>> Ciudad = "Madrid"
>> }

PS C:\> $myObject

Nombre  Edad Ciudad
------  ---- ------
Ejemplo   25 Madrid

PS C:\> $env:TMP
  C:\Users\Administrator\AppData\Local\Temp

PS C:\> $myObject | ConvertTo-Json | Out-File -FilePath "$env:TMP\myObjectFile.json"

PS C:\> type "$env:TMP\myObjectFile.json"
{
    "Nombre":  "Ejemplo",
    "Edad":  25,
    "Ciudad":  "Madrid"
}
```
