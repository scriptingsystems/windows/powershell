Fuente: https://lazyadmin.nl/powershell/powershell-profile/

PowerShell Profile Location, lo guarda la variable **$PROFILE**, que a su ves tambien tiene unos **NoteProperty** de interes como: CurrentUserAllHosts, CurrentUserCurrentHost, ...


## Step 1: Check if a profile exists already

To start with, Open PowerShell and check if the profile exists already:
```powershell	
Test-Path -Path $PROFILE
```


### Step 1.1: Add New Profile in PowerShell ISE: Alternative Approach

If you don’t have a PowerShell profile yet, you can easily create one. To create a new profile, follow these steps:
1. Open a PowerShell console or Windows PowerShell ISE.
2. Run the following command to create a new profile script file:
```powershell
> New-Item -ItemType File -Path $profile -Force
```

## Step 2: Adding aliases for common commands

This command creates a new file at the location specified by the **$profile** variable. The **-Force** parameter ensures that the file is created even if the directory does not exist.

You can now open the newly created profile file for editing by running the following command:
```powershell
> notepad $profile
```

This will open the profile file in Notepad or your default text editor, allowing you to start customizing your PowerShell environment.


If you frequently use specific commands, you can create aliases for them in your profile. Let’s use PowerShell to add content to your profile instead of opening it in Notepad, this time:
```powershell
> Add-Content -Path $PROFILE -Value 'Set-Alias -Name download -Value Invoke-WebRequest'
```


Reload Profile
```powershell
> . $profile
```
