Fuente: https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/enter-pssession?view=powershell-7.4

Port default: 5985



Habilitar **PSSession**

1. Configure a computer to receive remote commands
Fuente: https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/enable-psremoting?view=powershell-7.4
```powershell
> Enable-PSRemoting
```

2. Start an interactive session
Fuente: https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/enter-pssession?view=powershell-7.4
```powershell
> Enter-PSSession -ComputerName dc
```


```powershell
> $creds = Get-PSCredential
> Enter-PSSession -Credencial $creds -ComputerName Server2022.ceh.com
```

Por si diera fallo, consultar el siguiente enlace

Fuente: https://4sysops.com/archives/enable-powershell-remoting-on-a-standalone-workgroup-computer/
