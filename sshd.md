## How to install OpenSSH on Windows

we have to find out which versions of the software are available, which can be done with the command:
```powershell
> Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH*'
```

You should see the following printed out in your PowerShell terminal:
* **Name**: OpenSSH.Client~~~~0.0.1.0
  **State**: NotPresent
* **Name**: OpenSSH.Server~~~~0.0.1.0
  **State**: NotPresent

What the above means is neither the OpenSSH (version 0.0.1.0) client or server is installed. Let’s fix that. First, install the OpenSSH client with:
```powershell
> Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
```

Next, install the OpenSSH server with:
```powershell
> Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
```

Now, if you issue the command:
```powershell
> Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH*'
```

Both client and server should be listed as Installed.

Next, we need to start the OpenSSH server with the command:
```powershell
> Start-Service sshd
```

Now, we’ll set the OpenSSH server to start at boot. For that, issue:
```powershell
> Set-Service -Name sshd -StartupType 'Automatic'
```
